# Demande à l'utilisateur de saisir une phrase
sentence = input("Hello Man, Tape ta phrase stp : ")

# Demande à l'utilisateur de saisir un caractère
caractere = input("Et maintenant, Tape un caractère stp : ")

# Compte le nombre de caractère dans la phrase
Nb_de_caractere = len(sentence)

# Initialise une variable pour compter le caractère qui se répète
same_character_counter = 0

# Boucle, la variable position (qui prend une nouvelle valeur à chaque repetition)
# le nombre de fois correspondant si le caractère est dans la phrase
for position, char in enumerate(sentence):
    if char == caractere:

    # Si i (numéro de répétition de la boucle donc de position du caractère dans la phrase)
    # est egal au caractere,

        # alors on affiche la position du caractère
        print("On retrouve le caractère", caractere, "à la position", position)

        # On incrémente le compteur de même caractère et on boucle
        same_character_counter += 1

# Vérifie s'il y a eu des occurrences du caractère
if same_character_counter == 0:
    print("Le caractère", caractere, "n'est pas présent dans la phrase")
